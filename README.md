# RSS Feed Reader

RSS feed reader app for managing RSS feed links and viewing feed contents.

## Installation
- Upload files to a server, open a command prompt in the root folder and enter `composer install`
- Create a database and enter the database credentials in the `config/config.php` file.
- On a production server, set the virtual host root directory to `public`

## Testing
The tests use an alternative database to preserve existing data. The test database credentials can be set in the `config/config_test.php` file.

## Future improvements
- Test that URL is for an RSS XML file
- More flexible/ robust parsing of the XML items