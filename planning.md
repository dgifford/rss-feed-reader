# Planning Process

- A simple sketch of UI to help visualize functionality
- Initial list of classes required for a basic MVC structure:
  - `App` Controller (and user input validation)
  - `Template` View (combines HTML and app data)
  - `Feed`, `Reader` Models (`Reader` manages array of feeds, `Feed` an individual feed URL)
  - `DB` An abstract class or interface/trait providing the database connection methods for the models?
- Methods required:
  - `Reader` 
    - `add` create a `Feed` model and add it to the list
    - `delete` call delete method on `Feed` model to delete from DB and remove from list
    - `save` modify existing `Feed` model URL and save it to DB
    - `load` call method on `Feed` model to load XML and parse into articles
  - `Feed`
    - `save` run update or insert on DB
    - `delete` delete from DB
    - `load` load XML from feed URL
- Set up tests for model methods with expected output
- Folder structure:
    - `public` publically accessible folder
    - `src` models and classes
    - `config` configuration files
    - `sql` MySQL queries for generating DB tables and test data
    - `tests` phpunit tests





