<?php
Namespace dgifford\Reader;



/*
	Database connection injection
 */



abstract class DBConnector
{
	/**
	 * PHP PDO class object
	 * 
	 * @var \PDO
	 */
	protected $pdo;



	/**
	 * PDO object injected via constructor.
	 * 
	 * @param \PDO $pdo
	 */
	public function __construct( \PDO $pdo )
	{
		$this->setPDO( $pdo );
	}



	/**
	 * Setter for injecting PDO object.
	 * 
	 * @param \PDO $pdo
	 */
	public function setPDO( \PDO $pdo )
	{
		$this->pdo = $pdo;
	}
}