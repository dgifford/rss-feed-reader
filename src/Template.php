<!doctype html>
<html lang="en">
	<head>
		<title>RSS Reader</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" type="text/css" href="https://necolas.github.io/normalize.css/8.0.0/normalize.css" media="screen">
		<link rel="stylesheet" type="text/css" href="styles.css" media="screen">
	</head>



	<body>
		<h1>RSS Reader</h1>

		<div class="column feed_list">
			<div class="messages">
				<?php foreach( $app->messages as $message ) { ?>
					<p><?php echo $message; ?></p>
				<?php } ?>
			</div>

			<ul class="">
				<?php foreach( $app->reader->feeds as $index => $feed ) { ?>
				<li>
					<form id="feed" method="post" action="">
						<div class="url">
							<input name="url" value="<?php echo $feed->url; ?>">
						</div>

						<div class="controls">
							<input type="hidden" name="id" value="<?php echo $feed->id; ?>">
							<button type="submit" name="action" value="save">Save</button>
							<button type="submit" name="action" value="delete">Delete</button>
							<button type="submit" name="action" value="load">Load</button>
						</div>
					</form>		
				</li>
				<?php } ?>
			</ul>

			<div>
				<h2>Add RSS feed</h2>
				<form id="new" method="post" action="">
					<input type="url" name="url" placeholder="Feed URL" required="true">
					<button type="submit" name="action" value="add">Add</button>
				</form>
			</div>			
		</div>



		<div class="column articles">
			<ul class="">
				<?php foreach( $app->reader->articles as $index => $article ) { ?>
				<li>
					<h3><a href="<?php echo $article['link']; ?>"><?php echo $article['title']; ?></a><span class="date"><?php echo $article['date']; ?></span></h3>

					<p><?php echo $article['description']; ?></p>
				</li>

				<?php } ?>
			</ul>
		</div>





	</body>
</html>