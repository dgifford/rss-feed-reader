<?php
Namespace dgifford\Reader;



/*
 */


class Feed extends DBConnector
{
	public $id;

	public $created;

	public $modified;

	public $url;




	/**
	 * 	Set DB connection and optionally the feed URL
	 * 
	 * @param \PDO $pdo
	 */
	public function __construct( \PDO $pdo, $url = null )
	{
		$this->setPDO( $pdo );

		if( isset( $url ) )
		{
			$this->setURL( $url );
		}
	}



	/**
	 * Save the feed to the DB. If the feed has an ID
	 * update otherwise insert.
	 * 
	 * @return null
	 */
	public function save()
	{
		// Update
		if( isset( $this->id ) )
		{
			$this->update();
		}
		else
		// Insert
		{
			$this->insert();
		}
	}



	/**
	 * Update the feed in the DB.
	 * 
	 * @return null
	 */
	protected function update()
	{
		$this->pdo->prepare('UPDATE `feeds` SET `modified` = ?, `url` = ? WHERE `feeds`.`id` = ?')->execute([ $this->getNow(), $this->url, $this->id ]);
	}



	/**
	 * Insert the feed in the DB, retrieve the ID of the inserted
	 * record and set in model.
	 * 
	 * @return null
	 */
	protected function insert()
	{
		$this->pdo->prepare('INSERT INTO `feeds` (`id`, `created`, `modified`, `url`) VALUES (NULL, ?, ?, ?);')->execute([ $this->getNow(), $this->getNow(), $this->url ]);
		$this->id = $this->pdo->lastInsertId();
	}



	/**
	 * Delete the feed in the DB.
	 * 
	 * @return null
	 */
	public function delete()
	{
		$this->pdo->prepare('DELETE FROM `feeds` WHERE `feeds`.`id` = ?')->execute([ $this->id ]);
	}



	/**
	 * Set the URL of the feed.
	 * 
	 * @return null
	 */
	public function setURL( $url )
	{
		$this->url = $url;
	}



	/**
	 * Load the XML for this feed.
	 * 
	 * @return null
	 */
	public function load()
	{
		$this->xml = new \DOMDocument();

		$this->xml->load( $this->url );
	}



	/**
	 * Return a datetime stamp for now.
	 * 
	 * @return null
	 */
	protected function getNow()
	{
		return date('Y-m-d H:i:s');
	}

}