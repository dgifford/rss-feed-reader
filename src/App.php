<?php
Namespace dgifford\Reader;



/*
	App object holds app level variables and acts as a controller, validator and router
 */


class App extends DBConnector
{
	/**
	 * The Reader object
	 * @var Reader
	 */
	public $reader;



	/**
	 * Array of valid user input
	 * @var array
	 */
	protected $input = [];



	/**
	 * Array of messages for user.
	 * @var array
	 */
	public $messages = [];






	/**
	 * Set DB connection and Reader, validate input
	 * and control actions.
	 * 
	 * @param \PDO $pdo 	PDO DB connection
	 * @param array $input 	User input
	 */
	public function __construct( \PDO $pdo, array $input = [] )
	{
		$this->setPDO( $pdo );

		$this->reader = new Reader( $pdo );

		$this->sanitize( $input );
	}



	/**
	 * Sanitize input.
	 * 
	 * @param  array $input  
	 * @return null
	 */
	protected function sanitize( array $input )
	{
		if( !empty($input) )
		{
			$this->input = filter_var_array($input, 
			[
				'id' 		=> FILTER_VALIDATE_INT,
				'action'	=> FILTER_SANITIZE_STRING,
				'url' 		=> FILTER_VALIDATE_URL,
			]);

			// Generate user messages
			if( isset($this->input['id']) and !$this->validID() )
			{
				$this->addMessage( 'The feed does not exist.' );
			}

			if( !$this->validAction() )
			{
				$this->generalUserError();
			}

			if( isset($this->input['url']) and !$this->validURL() )
			{
				$this->addMessage( 'The URL is invalid.' );
			}
		}
	}



	/**
	 * Boolean check if a feed id is valid
	 * 
	 * @param  integer $id
	 * @return boolean
	 */
	protected function validID()
	{
		if( !is_int($this->input['id']) or $this->reader->getFeed( $this->input['id'] ) === false )
		{
			return false;
		}

		return true;
	}



	/**
	 * Boolean check if a Reader action is valid
	 * 
	 * @param  string $action
	 * @return boolean
	 */
	protected function validAction()
	{
		if( !is_string($this->input['action']) or !is_callable([ $this->reader, $this->input['action'] ]) )
		{
			return false;
		}

		return true;
	}



	/**
	 * Boolean check if a URL is valid
	 * 
	 * @param  string $url
	 * @return boolean
	 */
	protected function validURL()
	{
		if( !is_string($this->input['url']) or filter_var($this->input['url'], FILTER_VALIDATE_URL) === false )
		{
			return false;
		}

		return true;
	}



	/**
	 * Control the App based on user input.
	 * 
	 * @return null
	 */
	public function controller()
	{
		if( isset($this->input['action']) )
		{
			switch( $this->input['action'] )
			{
				case 'add':
					if( $this->validURL() )
					{
						$this->reader->add( $this->input['url'] );
					}
					else
					{
						$this->generalUserError();
					}
				break;
				
				case 'delete':
					if( $this->validID() )
					{
						$this->reader->delete( $this->input['id'] );
					}
					else
					{
						$this->generalUserError();
					}
				break;
				
				case 'save':
					if( $this->validID() and $this->validURL() )
					{
						$this->reader->save( $this->input['id'], $this->input['url'] );
					}
					else
					{
						$this->generalUserError();
					}
				break;
				
				case 'load':
					if( $this->validID() )
					{
						$this->reader->load( $this->input['id'] );
					}
					else
					{
						$this->generalUserError();
					}
				break;
			}
		}
	}



	/**
	 * Add a general error message.
	 * 
	 * @return null
	 */
	protected function generalUserError()
	{
		$this->addMessage( 'There was a problem, please try again.' );
	}



	/**
	 * Add a message to display to the user.
	 * 
	 * @param null	 */
	protected function addMessage( $message )
	{
		if( !in_array( $message, $this->messages ) )
		{
			$this->messages[] = $message;
		}
	}

}