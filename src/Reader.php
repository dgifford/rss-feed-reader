<?php
Namespace dgifford\Reader;



/*
 */


class Reader extends DBConnector
{
	/**
	 * Container for feed objects.
	 * 
	 * @var array
	 */
	public $feeds = [];



	/**
	 * Array of selected feed articles.
	 * @var array
	 */
	public $articles = [];






	/**
	 * Set DB connection and get feed list from DB 
	 * 
	 * @param \PDO $pdo
	 */
	public function __construct( \PDO $pdo )
	{
		$this->setPDO( $pdo );

		$this->getList();
	}



	/**
	 * Get all feed object from database into the feeds array
	 * 
	 * @return null
	 */
	protected function getList()
	{
		$this->feeds = $this->pdo->query('SELECT * FROM feeds')->fetchAll(\PDO::FETCH_CLASS, 'dgifford\Reader\Feed', [$this->pdo] );
	}



	/**
	 * Return the number of feeds.
	 * 
	 * @return integer
	 */
	public function count()
	{
		return count($this->feeds);
	}



	/**
	 * Given an id, return a feed from the feeds array
	 * or false if it is not present.
	 * 
	 * @param  int    			$id
	 * @return Feed/ boolean 	Feed object or false
	 */	
	public function getFeed( int $id )
	{
		foreach( $this->feeds as $feed )
		{
			if( $feed->id == $id )
			{
				return $feed;
			}
		}

		return false;
	}



	/**
	 * Return the index of a given feed id.
	 * 
	 * @param  int    $id
	 * @return integer/ boolean
	 */
	public function getIndex( int $id )
	{
		foreach( $this->feeds as $index => $feed )
		{
			if( $feed->id == $id )
			{
				return $index;
			}
		}

		return false;
	}



	/**
	 * Check if a feed exists with a given URL.
	 * 
	 * @param  string $url
	 * @return boolean
	 */
	public function feedInList( $url )
	{
		foreach( $this->feeds as $feed )
		{
			if( $feed->url == $url )
			{
				return true;
			}
		}

		return false;
	}



	/**
	 * Add a feed URL. If the URL already exists,
	 * return false.
	 * 
	 * @param  string $url 	A valid URL
	 */
	public function add( $url )
	{
		if( $this->feedInList( $url ) )
		{
			return "The URL {$url} already exists.";
		}
		else
		{
			$feed = new Feed( $this->pdo, $url );
			$feed->save();

			// Update feed list
			$this->getList();
		}
	}



	/**
	 * Delete a field and remove it from the feeds list.
	 * 
	 * @param  integer $id
	 * @return null
	 */
	public function delete( $id )
	{
		$index = $this->getIndex( $id );

		$this->feeds[ $index ]->delete();

		unset($this->feeds[ $index ]);
	}



	/**
	 * Save changes to the URL of a feed.
	 * 
	 * @param  integer $id
	 * @return null
	 */
	public function save( $id, $url )
	{
		$index = $this->getIndex( $id );
		
		$this->feeds[ $index ]->setURL( $url );

		$this->feeds[ $index ]->save();
	}



	/**
	 * Load the articles for a given feed.
	 * 
	 * @param  integer $id
	 * @return null
	 */
	public function load( $id )
	{
		$index = $this->getIndex( $id );
		
		$this->feeds[ $index ]->load();

		foreach( $this->feeds[ $index ]->xml->getElementsByTagName('item') as $node )
		{
			$this->articles[] =
			[
				'title' 		=> $node->getElementsByTagName('title')->item(0)->nodeValue,
				'description' 	=> strip_tags( $node->getElementsByTagName('description')->item(0)->nodeValue ),
				'link' 			=> $node->getElementsByTagName('link')->item(0)->nodeValue,
				'date' 			=> ($node->getElementsByTagName('pubDate')->length > 0) ? $node->getElementsByTagName('pubDate')->item(0)->nodeValue : '',
			];
		}
	}
}