<?php
Namespace dgifford\Reader;



/**
 * Run tests on DB after running MySQL script sql/test_data.sql
 */




class Base extends \PHPUnit\Framework\TestCase
{
	public function getPDO()
	{
		$this->config = require __DIR__ . '/../config/config_test.php';

		return new \PDO( $this->config['dsn'], $this->config['user'], $this->config['password'] );
	}



	public function resetDB()
	{
		$this->getPDO()->query( file_get_contents( __DIR__ . '/../sql/test_data.sql') );
	}

}