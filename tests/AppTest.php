<?php
Namespace dgifford\Reader;



/**
 * WARNING 
 *
 * TESTS OVERWRITE EXISTING DATA
 *
 * Change config in /tests/Base.php to use a different database to preserve data
 */



/**
 * Auto Loader
 * 
 */
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/Base.php';



class AppTest extends Base
{
	public function setUp()
	{
		$this->resetDB();
	}



	public function createApp( $input = [] )
	{
		$this->app = new App( $this->getPDO(), $input );
	}



	public function testCreateApp()
	{
		$this->createApp();

		$this->assertTrue( $this->app instanceof App );

		$this->assertTrue( $this->app->reader instanceof Reader );
	}



	public function testInvalidID()
	{
		$this->createApp(['id' => 'foo']);

		$this->assertTrue( in_array('The feed does not exist.', $this->app->messages) );

		$this->createApp(['id' => 100]);

		$this->assertTrue( in_array('The feed does not exist.', $this->app->messages) );
	}



	public function testValidIndex()
	{
		$this->createApp(['id' => 1, 'action' => 'load']);

		$this->assertTrue( empty($this->app->messages) );

		$this->createApp(['id' => '1', 'action' => 'load']);

		$this->assertTrue( empty($this->app->messages) );
	}



	public function testInvalidOrNoAction()
	{
		$this->createApp(['action' => 'foo']);

		$this->assertTrue( in_array('There was a problem, please try again.', $this->app->messages) );

		$this->createApp(['id' => 0]);

		$this->assertTrue( in_array('There was a problem, please try again.', $this->app->messages) );
	}



	public function testInvalidURL()
	{
		$this->createApp(['url' => 'foo']);

		$this->assertTrue( in_array('The URL is invalid.', $this->app->messages) );
	}



	public function testController()
	{
		$this->createApp(['id' => 1, 'action' => 'load']);

		$this->app->controller();

		$this->assertTrue( !empty($this->app->reader->articles) );
	}

}