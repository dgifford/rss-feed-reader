<?php
Namespace dgifford\Reader;



/**
 * WARNING 
 *
 * TESTS OVERWRITE EXISTING DATA
 *
 * Change config in /tests/Base.php to use a different database to preserve data
 */



/**
 * Auto Loader
 * 
 */
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/Base.php';


class ReaderTest extends Base
{
	public function setUp()
	{
		$this->resetDB();

		$this->reader = new Reader( $this->getPDO() );
	}



	public function testCreateReader()
	{
		$this->assertTrue( $this->reader instanceof Reader );

		$this->assertSame( 3, $this->reader->count() );
	}



	public function testGetFeed()
	{
		$feed = $this->reader->getFeed(1);

		$this->assertTrue( $feed instanceof Feed );

		$this->assertSame( 'http://www.php.net/news.rss', $feed->url );
	}



	public function testFeedInList()
	{
		$this->assertTrue( $this->reader->feedInList('http://www.php.net/news.rss') );
	}



	public function testAddFeed()
	{
		$this->reader->add('https://google.com');

		$this->assertSame( 4, $this->reader->count() );
	}



	public function testDeleteFeed()
	{
		$this->reader->delete( 3 );

		$this->assertSame( 2, $this->reader->count() );
	}



	public function testSaveFeed()
	{
		$this->reader->save( 1, 'https://google.com' );

		$this->assertTrue( $this->reader->feedInList( 'https://google.com' ) );

		$this->reader->save( 1, 'http://www.php.net/news.rss' );

		$this->assertFalse( $this->reader->feedInList( 'https://google.com' ) );
	}



	public function testLoadFeed()
	{
		$this->reader->load( 1 );

		$this->assertTrue( count($this->reader->articles) > 0 );
	}






}