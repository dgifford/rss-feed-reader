<?php
Namespace dgifford\Reader;



/**
 * WARNING 
 *
 * TESTS OVERWRITE EXISTING DATA
 *
 * Change config in /tests/Base.php to use a different database to preserve data
 */



/**
 * Auto Loader
 * 
 */
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/Base.php';



class FeedTest extends Base
{
	public function testCreateFeedWithURL()
	{
		$feed = new Feed( $this->getPDO(), 'https://google.com' );

		$this->assertTrue( $feed instanceof Feed );

		$this->assertSame( $feed->url, 'https://google.com' );
	}
}