<?php 


error_reporting(E_ALL);
ini_set('display_errors', 1);


/**
 * Composer Auto Loader
 */
require_once __DIR__ . '/../vendor/autoload.php';



/**
 * Load config
 */
$config = require '../config/config.php';



/**
 * Database connection
 */
$pdo = new \PDO( $config['dsn'], $config['user'], $config['password'] );



/**
 * Create App
 */
$app = new dgifford\Reader\App( $pdo, $_POST );


/**
 * Call app Controller
 */
$app->controller();



/**
 * Render 
 */
require __DIR__ . '/../src/Template.php';