--
-- Table structure for table `feeds`
--

DROP TABLE IF EXISTS `feeds`;

CREATE TABLE `feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `url` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feeds`
--


INSERT INTO `feeds` VALUES (1,'2018-09-14 00:00:00','2018-09-14 00:00:00','http://www.php.net/news.rss'),(2,'2018-09-14 00:00:00','2018-09-14 00:00:00','http://slashdot.org/rss/slashdot.rss'),(3,'2018-09-14 00:00:00','2018-09-14 00:00:00','http://newsrss.bbc.co.uk/rss/newsonline_uk_edition/front_page/rss.xml');
